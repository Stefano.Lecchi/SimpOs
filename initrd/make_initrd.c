/**
  Utility script to insert files in "initrd.img", which is the
  image file which will put the initrd in our kernel.
**/

#include <stdio.h>

struct initrd_header
{
    unsigned char magic;          // Checking consistency.
    char name [64];
    unsigned int offset;          // Offset in the initrd.
    unsigned int length;          // Length of the file.
};

int main(char argc, char **argv)
{
    int n_headers = (argc - 1) / 2;

    struct initrd_header headers [64];

    printf("Size of header : %d\n", sizeof(struct initrd_header));

    unsigned int offset = sizeof(struct initrd_header) * 64 + sizeof(int);

    int i;
    for (i = 0; i < n_headers; i ++)
    {
        printf("Writing file %s -> %s at 0x%x\n", argv [i * 2 + 1], argv [i * 2 + 2], offset);

        strcpy(headers [i].name, argv [i * 2 + 2]);
        headers [i].offset = offset;

        FILE *stream = fopen(argv [i * 2 + 1], "r");

        if (stream == 0)
        {
              printf("Error : file not found %s\n", argv [i * 2 + 1]);
              return 1;
        }

        fseek(stream, 0, SEEK_END);

        headers [i].length = ftell(stream);
        offset += headers [i].length;

        fclose(stream);

        headers [i].magic = 0xBF;
    }

    FILE *wstream = fopen("./initrd.img", "w");

    unsigned char *data = (unsigned char *)malloc(offset);

    fwrite(&n_headers, sizeof(int), 1, wstream);
    fwrite(headers, sizeof(struct initrd_header), 64, wstream);

    for (i = 0; i < n_headers; i ++)
    {
        FILE *stream = fopen(argv [i * 2 + 1], "r");

        unsigned char *buffer = (unsigned char *)malloc(headers [i].length);

        fread(buffer, 1, headers [i].length, stream);
        fwrite(buffer, 1, headers [i].length, wstream);
        fclose(stream);
        free(buffer);
    }

    fclose(wstream);
    free(data);

    return 0;
}

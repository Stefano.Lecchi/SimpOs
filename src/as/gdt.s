; Routine to initialize the "Global Descriptor Table"

[GLOBAL gdt_flush]        ; Allowing the C code to call "gdt_flush()".

gdt_flush:

  mov eax, [esp + 4]      ; Getting the pointer to the GDT, passed as a parameter.
  lgdt [eax]              ; Loading the new GDT pointer.

  mov ax, 0x10            ; 0x10 is the offset in the GDT to our Data Segment.
  mov ds, ax              ; Loading all Data Segment selectors --->
  mov es, ax
  mov fs, ax
  mov gs, ax
  mov ss, ax
  jmp 0x08: .flush        ; 0x08 is the offset to our Code Segment.

.flush:

  ret

[GLOBAL idt_flush]        ; Allowing the C code to call "idt_flush()".

idt_flush:

  mov eax, [esp + 4]      ; Getting the pointer to the IDT, passed as a parameter.
  lidt [eax]              ; Loading the IDT pointer.
  ret

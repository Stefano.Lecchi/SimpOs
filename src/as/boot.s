; Definition of the kernel start location, and multiboot header.

MBOOT_PAGE_ALIGN    equ 1<<0            ; Load kernel and modules on a page boundary.
MBOOT_MEM_INFO      equ 1<<1            ; Providing memory info to the kernel.
MBOOT_HEADER_MAGIC  equ 0x1BADB002      ; Multiboot magic value. Your best friend to boot without your own bootloader.

MBOOT_HEADER_FLAGS  equ MBOOT_PAGE_ALIGN | MBOOT_MEM_INFO             ; Making an "OR" operation between the 2 labels.
MBOOT_CHECKSUM      equ -(MBOOT_HEADER_MAGIC + MBOOT_HEADER_FLAGS)    ; Calculating the checksum between the 2 labels (the total must be 0).


[BITS 32]                 ; Declaring that all the instructions should be 32 bit.

[GLOBAL mboot]            ; Making "mboot" accessible to "C" code.
[EXTERN code]             ; Start of the ".text" section
[EXTERN bss]              ; Start of the ".bss" section
[EXTERN end]              ; End of the last loadable section.

mboot:

  dd MBOOT_HEADER_MAGIC     ; GRUB will search for this value on each 4-byte boundary of our kernel file.
  dd MBOOT_HEADER_FLAGS     ; Telling GRUB how to load our kernel file
  dd MBOOT_CHECKSUM         ; We need this to ensure that the above values are correct.

  dd mboot                  ; Location of this descriptor.
  dd code                   ; Start of kernel ".text(code)" section.
  dd bss                    ; End of kernel ".data" section.
  dd end                    ; End of kernel.
  dd start                  ; Kernel entry point (intial EIP).


[GLOBAL start]    ; Kernel entry point.
[EXTERN main]     ; Entry point for our "C" code.

start:

  push ebx      ; Loading multiboot header location

  ; Executing the kernel :
  cli           ; Disabling interrupts.
  call main     ; Calling our "main()" function.
  jmp $         ; Entering an infinite loop to stop the processor executing other things in memory after our kernel.

/**
  Defining the interface for the functions
  which control the monitor.
**/

#ifndef MONITOR_H
#define MONITOR_H

#include "common.h"

/**
  Write a single character out to the screen.
**/
void monitor_put(char c);

/**
  Clear the screen (clears everything).
**/
void monitor_clear();

/**
  Output a null-terminated ASCII string to the monitor_put
**/
void monitor_write(char *character);

#endif

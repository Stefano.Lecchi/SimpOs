/**
  Defining structures and prototypes for the initial ramdisk.
**/

#ifndef INITRD_H
#define INITRD_H

#include "common.h"
#include "fs.h"

typedef struct
{
    u32int n_files;           // Number of files in the ramdisk.
} initrd_header_t;

typedef struct
{
    u8int magic;                // Magic number for error checking.
    s8int name [64];            // File name.
    u32int offset;              // Offset in the ramdisk from the top.
    u32int length;              // Length of the file.
} initrd_file_header_t;

/**
  Initialization of the initrd. It gets passed the address of the
  multiboot module, and returns a completed filesystem node.
**/
fs_node_t *initialize_initrd(u32int location);

#endif

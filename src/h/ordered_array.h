/**
  Interface for creating an ordered array and inserting / deliting from it.
**/

#ifndef ORDERED_ARRAY_H
#define ORDERED_ARRAY_H

#include "common.h"

/**
  This array will always remain sorted when an insertion action will happen.
  It can store anything that can be cast to a void pointer (so u32int or any other pointer).
**/
typedef void* type_t;

/**
  The predicate should return a nonzero value if the first argument
  is less than the second. Else it should return zero.
  (A predicate is a boolean function; in our case it will not be
  true or false, but it will be zero or nonzero).
**/
typedef s8int (*less_than_predicate_t)(type_t, type_t);

typedef struct
{
    type_t *array;
    u32int size;
    u32int max_size;
    less_than_predicate_t less_than;
} ordered_array_t;


/**
  Declaring the "standard less than predicate".
**/
s8int standard_less_than_predicate(type_t a, type_t b);

/**
  Create ordered array functions.
**/
ordered_array_t create_ordered_array(u32int max_size, less_than_predicate_t less_than);
ordered_array_t place_ordered_array(void* addr, u32int max_size, less_than_predicate_t less_than);

/**
  Destroy an ordered array.
**/
void destroy_ordered_array(ordered_array_t *array);

/**
  Add an item into the array.
**/
void insert_ordered_array(type_t item, ordered_array_t *array);

/**
  Get the item at position i.
**/
type_t lookup_ordered_array(u32int i, ordered_array_t *array);

/**
  Delete the item at location i from the array.
**/
void remove_ordered_array(u32int i, ordered_array_t *array);


#endif 

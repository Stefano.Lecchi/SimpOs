/**
    Defining structs and prototypes for the FileSystem.
**/

#ifndef FS_H
#define FS_H

#include "common.h"

#define FS_FILE 0x01
#define FS_DIRECTORY 0x02
#define FS_CHARDEVICE 0x03
#define FS_BLOCKDEVICE 0x04
#define FS_PIPE 0x05
#define FS_SYMLINK 0x06
#define FS_MOUNTPOINT 0x08

typedef u32int (*read_type_t)(struct fs_node*, u32int, u32int, u8int*);
typedef u32int (*write_type_t)(struct fs_node*, u32int, u32int, u8int*);
typedef void (*open_type_t)(struct fs_node*);
typedef void (*close_type_t)(struct fs_node*);
typedef struct dirent * (*readdir_type_t)(struct fs_node*, u32int);
typedef struct fs_node * (*finddir_type_t)(struct fs_node*, char *name);

/**
  FileSystem's node struct.
**/
typedef struct fs_node
{
    char name [128];          // File name.
    u32int mask;              // Permission mask.
    u32int uid;               // Owning user.
    u32int gid;               // Owning group.
    u32int flags;             // Includes the node type (Directory, file etc...).
    u32int inode;             // Providing a way to the FS to identify files by assigning them a number.
    u32int length;            // Size of the file (in bytes).
    u32int impl;              // Implementation-defined number.
    read_type_t read;         // Pointer functions :
    write_type_t write;
    open_type_t open;
    close_type_t close;
    readdir_type_t readdir;
    finddir_type_t finddir;
    struct fs_node *ptr;      // Used by mountpoints and symlinks.
} fs_node_t;

struct dirent
{
    char name [128];          // File name.
    u32int ino;               // Inode number. Required by POSIX.
};

/**
  FS' root.
**/
extern fs_node_t *fs_root;

/**
  Standard read/write/open/close functions. These are suffixed with "_fs"
  to distinguish them from the read/write/open/close which deal with file descriptors, not file nodes.
**/
u32int read_fs(fs_node_t *node, u32int offset, u32int size, u8int *buffer);
u32int write_fs(fs_node_t *node, u32int offset, u32int size, u8int *buffer);
void open_fs(fs_node_t *node, u8int read, u8int write);
void close_fs(fs_node_t *node);
struct dirent *readdir_fs(fs_node_t *node, u32int index);
fs_node_t *finddir_fs(fs_node_t *node, char *name);

#endif

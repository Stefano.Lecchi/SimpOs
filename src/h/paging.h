/**
  Structures and functions to perform paging.
**/

#ifndef PAGING_H
#define PAGING_H

#include "common.h"
#include "isr.h"

typedef struct page
{
    u32int present : 1;             // Page present in memory.
    u32int rw : 1;                  // Read-only if 0. Read-Write if 1.
    u32int user : 1;                // Supervisor level if 0.
    u32int accessed : 1;            // Has the page been accessed since the last update?
    u32int dirty : 1;               // Has the page been written since the last update?
    u32int unused : 7;              // Unused and reserved bits.
    u32int frame : 20;              // Frame address (shifted right 12 bits).
} page_t;

typedef struct page_table
{
    page_t pages [1024];
} page_table_t;

typedef struct page_directory
{
    // Array of pointers to pagetables.
    page_table_t *tables [1024];

    // Array of pointers to the pagetables above, but gives their "physical"
    // location for loading into the CR3 register.
    u32int tables_physical [1024];

    // Physical address of "tables_physical". We'll use it
    // when we get our kernel heap allocated and the direcory may be
    // in a different location in virtual memory.
    u32int physical_address;
} page_directory_t;


/**
  Enabling paging.
**/
void initialize_paging();

/**
  Loading the specified page directory into the CR3 register.
**/
void switch_page_directory(page_directory_t *new);

/**
  Retrieving a pointer to the required page.
  If make == 1, and the page-table in which this page should reside doesn't exist,
  create it.
**/
page_t *get_page(u32int address, int make, page_directory_t *dir);

/**
  Handler for page faults.
**/
void page_fault(registers_t regs);

#endif

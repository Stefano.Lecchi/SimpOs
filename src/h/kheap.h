/**
  Interface for kernel heap functions.
  N.B ---> Right now it also provides a malloc() to use
  before we initilize the heap.
**/

#ifndef KHEAP_H
#define KHEAP_H

#include "common.h"
#include "ordered_array.h"

#define KHEAP_START 0xC0000000
#define KHEAP_INITIAL_SIZE 0x100000

#define HEAP_INDEX_SIZE 0x20000
#define HEAP_MAGIC 0x123890AB
#define HEAP_MIN_SIZE 0x70000

/**
  Size information for a Hole / Block.
**/
typedef struct
{
    u32int magic;           // Magic number used for error checking and identification.
    u8int is_hole;          // If 1, it is a Hole; if 0, it is a Block.
    u32int size;            // Size of the block including the footer.
} header_t;

typedef struct
{
    u32int magic;           // Magic number. Same as in "header_t".
    header_t *header;       // Pointer to the block's header.
} footer_t;

typedef struct
{
    ordered_array_t index;
    u32int start_address;       // The Start of our allocated space.
    u32int end_address;         // The end of our allocated space; may be extended to "max_address".
    u32int max_address;         // The maximum address the heap can be expanded to.
    u8int supervisor;           // Should requested extra pages be mapped as supervisor-only?
    u8int readonly;             // Should requested extra pages be mapped as read-only?
} heap_t;

/**
  Create a new heap;
**/
heap_t *create_heap(u32int start, u32int end, u32int max, u8int supervisor, u8int readonly);

/**
  Allocate a contiguous region of memory size in "size".
  If page_align = 1, it creates that block starting on a page boundary.
**/
void *alloc(u32int size, u8int page_align, heap_t *heap);

/**
  Release a block allocated with "alloc()".
**/
void free(void *p, heap_t *heap);

/**
  Allocating a chunk of memory of size sz.
  If align is equal to 1, the chunck must be page-aligned.
  If phys != 0, the physical location of the allocated chunk will be stored into phys.
**/
u32int kmalloc_int(u32int sz, int align, u32int *phys);

/**
  Allocating a chunk of memory of size sz. The chunk must be page-aligned.
**/
u32int kmalloc_a(u32int sz);

/**
  Allocating a chunk of memory of size sz. The physical address
  is returned in phys. Phys MUST BE A VALID POINTER TO u32int.
**/
u32int kmalloc_p(u32int sz, u32int *phys);

/**
  Allocating a chunck of memory of size sz. The physical address
  is returned in phys. The chunk must be page-aligned.
**/
u32int kmalloc_ap(u32int sz, u32int *phys);

/**
  General allocation.
**/
u32int kmalloc(u32int sz);

/**
  General deallocation function.
**/
void kfree(void *p);

#endif

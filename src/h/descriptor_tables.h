/**
  Declaring prototypes and structures
  to enable the descriptor tables.
**/

/********************************
*                               *
*         GDT Prototypes        *
*                               *
********************************/

#ifndef DESCRIPTOR_TABLES_H
#define DESCRIPTOR_TABLES_H

#include "common.h"

// Prototype : Initialization function to enable the descriptor tables.
void init_descriptor_tables();

/**
  The following structure contains the value of one GDT entry.
  The attribut "packed" is used to tell GCC not to change
  the alignments in the structure.
**/
struct gdt_entry_struct
{
    u16int limit_low;       // Lower 16 bits of the limit section.
    u16int base_low;        // Lower 16 bits of the base section.
    u8int base_middle;      // Base's next 8 bits.
    u8int access;           // Access flag; determine the ring level of the segmenmt.
    u8int granularity;
    u8int base_high;        // Last 8 bits of the base.
} __attribute__((packed));
typedef struct gdt_entry_struct gdt_entry_t;

/**
  This special pointer structure is used to tell the
  processor where to find our GDT.
**/
struct gdt_ptr_struct
{
    u16int limit;             // Size of the table minus 1.
    u32int base;              // Address of the first entry in the GDT.
} __attribute__((packed));
typedef struct gdt_ptr_struct gdt_ptr_t;


/********************************
*                               *
*         IDT Prototypes        *
*                               *
********************************/

struct idt_entry_struct
{
    u16int base_low;        // The lower 16 bits of the address to jump when an interrupt triggers.
    u16int selector;        // Kernel segment selector.
    u8int always_zero;      // This part must always be zero.
    u8int flags;            // Flags.
    u16int base_high;       // The upper 16 bits of the address to jump to.
}__attribute__((packed));
typedef struct idt_entry_struct idt_entry_t;


/**
  This struct describes a pointer to an array of interrupt handlers.
**/
struct idt_ptr_struct
{
    u16int limit;
    u32int base;            // This is the address in our array of interrupts.
}__attribute__((packed));
typedef struct idt_ptr_struct idt_ptr_t;


/**
  Extern directives which let us access the addresses of our ASM ISR handlers.
**/
extern void isr0();
extern void isr1();
extern void isr2();
extern void isr3();
extern void isr4();
extern void isr5();
extern void isr6();
extern void isr7();
extern void isr8();
extern void isr9();
extern void isr10();
extern void isr11();
extern void isr12();
extern void isr13();
extern void isr14();
extern void isr15();
extern void isr16();
extern void isr17();
extern void isr18();
extern void isr19();
extern void isr20();
extern void isr21();
extern void isr22();
extern void isr23();
extern void isr24();
extern void isr25();
extern void isr26();
extern void isr27();
extern void isr28();
extern void isr29();
extern void isr30();
extern void isr31();

extern void irq0();
extern void irq1();
extern void irq2();
extern void irq3();
extern void irq4();
extern void irq5();
extern void irq6();
extern void irq7();
extern void irq8();
extern void irq9();
extern void irq10();
extern void irq11();
extern void irq12();
extern void irq13();
extern void irq14();
extern void irq15();

#endif

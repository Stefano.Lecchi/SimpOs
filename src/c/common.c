/**
  Implementing the definition of the functions
  in "common.h".
**/

#include "../h/common.h"

/**
  Writing a byte to the specified port.
**/
void outb(u16int port, u8int value)
{
    // "outb" is the instruction to write a byte to a I/O port.
    asm volatile("outb %1, %0" : : "dN" (port), "a" (value));
}

u8int inb(u16int port)
{
    u8int ret;

    asm volatile("inb %1, %0" : "=a" (ret) : "dN" (port));

    return ret;
}

u16int inw(u16int port)
{
    u16int ret;

    asm volatile("inw %1, %0" : "=a" (ret) : "dN" (port));

    return ret;
}

/**
  Copying the values at memory location *source" to memory location ""*dest".
**/
void memcpy(void *destination, const void *source, int count)
{
    char *c_dest = (char *) destination;
    char *c_src = (char *) source;

    for(; count != 0; count --)
        *c_dest ++ = *c_src ++;

    return destination;
}

/**
  Setting the first "count" values of memory location "*destination" equals to "value".
**/
void memset(void *destination, int value, int count)
{
    char *c_dest = (char *) destination;

    for(; count != 0; count --)
        *c_dest ++ = (char) value;

    return destination;
}

/**
  Doing the same as "memset", but working with 16-bit values (char = 8-bit, short = 16-bit).
**/
unsigned short *memsetw(unsigned short *destination, unsigned short value, int count)
{
    unsigned short *c_dest = (unsigned char *) destination;

    for(; count != 0; count --)
        *c_dest ++ = value;

    return destination;
}

// Compare two strings. Should return -1 if
// str1 < str2, 0 if they are equal or 1 otherwise.
int strcmp(char *str1, char *str2)
{
      int i = 0;
      int failed = 0;
      while(str1[i] != '\0' && str2[i] != '\0')
      {
          if(str1[i] != str2[i])
          {
              failed = 1;
              break;
          }
          i++;
      }
      // why did the loop exit?
      if( (str1[i] == '\0' && str2[i] != '\0') || (str1[i] != '\0' && str2[i] == '\0') )
          failed = 1;

      return failed;
}

// Copy the NULL-terminated string src into dest, and
// return dest.
char *strcpy(char *dest, const char *src)
{
    do
    {
      *dest++ = *src++;
    }
    while (*src != 0);
}

// Concatenate the NULL-terminated string src onto
// the end of dest, and return dest.
char *strcat(char *dest, const char *src)
{
    while (*dest != 0)
    {
        *dest = *dest++;
    }

    do
    {
        *dest++ = *src++;
    }
    while (*src != 0);
    return dest;
}

int strlen(char *src)
{
    int i = 0;
    while (*src++)
        i++;

    return i;
}

extern void panic(const char *message, const char *file, u32int line)
{
    asm volatile("cli");

    monitor_write("PANIC(");
    monitor_write(message);
    monitor_write(") at ");
    monitor_write(file);
    monitor_write(":");
    monitor_write_dec(line);
    monitor_write("\n");
    // Halt by going into an infinite loop.
    for(;;);
}

extern void panic_assert(const char *file, u32int line, const char *desc)
{
    asm volatile("cli");

    monitor_write("ASSERTION-FAILED(");
    monitor_write(desc);
    monitor_write(") at ");
    monitor_write(file);
    monitor_write(":");
    monitor_write_dec(line);
    monitor_write("\n");
    // Halt by going into an infinite loop.
    for(;;);
}

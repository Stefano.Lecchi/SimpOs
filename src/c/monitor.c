/**
  Implementing the definition of the functions
  in "monitor.h".
**/
#include "../h/monitor.h"

// The VGA framebuffer starts at 0xB8000.
u16int *video_memory = (u16int *)0xB8000;

// Variables to store the cursor position.
u8int cursor_x = 0;
u8int cursor_y = 0;

// Lines and coloumns of the screen.
const int SCREEN_LINES = 25;
const int SCREEN_COLOUMNS = 80;

// The following funtions are "static" functions, so we are implementing
// them here and not difining them in the header file.

/**
  Updating the hardware cursor.
**/
static void move_cursor()
{
    // IMPORTANT : The screen is 80 characters wide.
    u16int cursor_location = (cursor_y * SCREEN_COLOUMNS) + cursor_x;

    // Telling the VGA hardware we are setting the high cursor byte.
    outb(0x3D4, 14);
    // Sending the high cursor byte.
    outb(0x3D5, cursor_location >> 8);
    // Telling the VGA hardware we are setting the low cursor byte.
    outb(0x3D4, 15);
    // Sending the low cursor byte.
    outb(0x3D5, cursor_location);
}

/**
  Scrolling the text on screen up by one line.
**/
static void scroll()
{
    // Getting a space character with the default color attributes.
    u8int attribute_byte = (0 << 4) | (15 & 0x0F);    // 0 = black, 15 = white.
    u16int blank = 0x20 | (attribute_byte << 8);      // 0x20 = space.

    // If we reach row 25, we need to scrool up
    if (cursor_y >= SCREEN_LINES)
    {
        // Move the current amount of text which occupies the screen
        // back in the buffer by one line.
        int i;
        for (i = 0 * SCREEN_COLOUMNS; i < (SCREEN_LINES - 1) * SCREEN_COLOUMNS; i++)
        {
            video_memory [i] = video_memory [i + SCREEN_COLOUMNS];
        }

        // The last line should be blank. Doing this
        // by writing 80 spaces to it.
        for(i = (SCREEN_LINES - 1) * SCREEN_COLOUMNS; i < SCREEN_LINES * SCREEN_COLOUMNS; i++)
        {
            video_memory [i] = blank;
        }

        // Setting the cursor to be on the last line.
        cursor_y = SCREEN_LINES - 1;
    }
}


/**
  Writing a single character out to the screen.
**/
void monitor_put(char character)
{
    // Background color = black (0) --- Foreground color = white (15).
    u8int background_color = 0;
    u8int foreground_color = 15;

    // The attribute byte is made up of two nibbles - the lower being the
    // foreground colour, and the upper the background colour.
    u8int attribute_byte = (background_color << 4) | (foreground_color & 0x0F);

    // The attribute byte is the top 8 bits of the word we have to send to the VGA hardware.
    u16int attribute = attribute_byte << 8;
    u16int *location;

    // Handling a backspace by moving the cursor back by one space.
    if (character == 0x08 && cursor_x)
    {
        cursor_x --;
    }
    // Handling a tab by increasing the cursor's X, but only to a point
    // where it is divisible by 8.
    else if (character == 0x09)
    {
        cursor_x = (cursor_x + 8) & ~(8 - 1);
    }
    // Handling carriage return.
    else if (character == '\r')
    {
        cursor_x = 0;
    }
    // Handling a new line.
    else if (character == '\n')
    {
        cursor_x = 0;
        cursor_y ++;
    }
    // Handling any other printable character.
    else if (character >= ' ')
    {
        // Setting "location" to point to the linear address of the word corresponding to the current cursor position.
        location = video_memory + (cursor_y * SCREEN_COLOUMNS) + cursor_x;
        // Setting the value of "location" to be the logical "OR" of the character and "attribute".
        *location = character | attribute;
        cursor_x ++;
    }

    // Checking if we need to insert a new line because we reached the end of the screen.
    if(cursor_x >= SCREEN_COLOUMNS)
    {
        cursor_x = 0;
        cursor_y ++;
    }

    // Scrolling the screen (if needed).
    scroll();

    // Moving the cursor.
    move_cursor();
}

/**
  Clearing the screen by copying lots of spaces to the frame buffer.
**/
void monitor_clear()
{
    // Making an attribute byte for the default color.
    u8int attribute_byte = (0 << 4) | (15 & 0x0F);
    u16int blank = 0x20 | (attribute_byte << 8);

    int i;
    for(i = 0; i < SCREEN_COLOUMNS * SCREEN_LINES; i ++)
    {
        video_memory [i] = blank;
    }

    cursor_x = 0;
    cursor_y = 0;
    move_cursor();
}

/**
  Writing a null-terminated ASCII string to the monitor.
**/
void monitor_write(char *character)
{
    int i;

    for(i = 0; character [i] != '\0'; i++)
    {
        monitor_put(character [i]);
    }
}

/**
  Writing an Hex value to the monitor.
**/
void monitor_write_hex(u32int n)
{
   s32int temp;

   monitor_write("0x");

   char noZeroes = 1;

   int i;
   for(i = 28; i > 0; i -= 4)
   {
      temp = (n >> i) & 0xF;

      if (temp == 0 && noZeroes != 0)
          continue;

      if (temp >= 0xA)
      {
          noZeroes = 0;
          monitor_put(temp - 0xA + 'a');
      }
      else
      {
          noZeroes = 0;
          monitor_put(temp + '0');
      }
   }

   temp = n & 0xF;

   if (temp >= 0xA)
      monitor_put(temp - 0xA + 'a');
   else
      monitor_put(temp + '0');
}

/**
  writing a decimal value to the monitor.
**/
void monitor_write_dec(u32int n)
{
   if(n == 0)
   {
      monitor_put('0');
      return;
   }

   s32int acc = n;
   char c [32];

   int i = 0;
   while(acc > 0)
   {
      c [i] = '0' + acc % 10;
      acc /= 10;
      i ++;
   }

   c [i] = 0;

   char c2 [32];
   c2 [i--] = 0;

   int j = 0;
   while(i >= 0)
   {
      c2 [i--] = c [j++];
   }

   monitor_write(c2);
}

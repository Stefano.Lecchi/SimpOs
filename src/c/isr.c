/**
  Implementing high level ISRs and interrupt request handlers.
**/
#include "../h/common.h"
#include "../h/isr.h"
#include "../h/monitor.h"

isr_t interrupt_handlers [256];

/**
  Registers an interrupt handler in the ISR table.
**/
void register_interrupt_handler(u8int n, isr_t handler)
{
    interrupt_handlers [n] = handler;
}

/**
  Support functions to debug the interrupt.
**/
void isr_handler(registers_t regs)
{
    if(interrupt_handlers [regs.int_no] != 0)
    {
        isr_t handler = interrupt_handlers [regs.int_no];
        handler(regs);
    }
    else
    {
      monitor_write("Received interrupt: ");
      monitor_write_dec(regs.int_no);
      monitor_put('\n');
    }
}

void irq_handler(registers_t regs)
{
    // Send an EOI (End Of Interrupt) signal to the PICs.
    // If the slave was involved, send a reset signal to it.
    if(regs.int_no >= 40)
    {
        outb(0xA0, 0x20);
    }

    // Sending reset signal to the master
    outb(0x20, 0x20);

    if(interrupt_handlers [regs.int_no] != 0)
    {
        isr_t handler = interrupt_handlers [regs.int_no];
        handler(regs);
    }
}

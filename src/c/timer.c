/**
  Initialization of the PIT, and handling clock updates.
**/
#include "../h/timer.h"
#include "../h/monitor.h"
#include "../h/isr.h"

u32int tick = 0;
u32int input_clock = 1193180;

/**
  This function gets called when a timer interrupt occurs.
**/
static void timer_callback(registers_t regs)
{
    tick ++;

    monitor_write("Tick: ");
    monitor_write_dec(tick);
    monitor_write("\n");
}

void init_timer(u32int frequency)
{
    // Registering our timer callback.
    register_interrupt_handler(IRQ0, &timer_callback);

    // We send to the PIT the value to divide its input clock by, to get the required
    // frequency. N.B ---> The divisor must be small enough to fit into 16 bits.
    u32int divisor = input_clock / frequency;

    // Sending the command to the PIT.
    // Setting the PIT to "repeting mode (when divisor's counter reaches zero, it's automatically refreshed)",
    // and telling to the PIT that we want to set the divisor value.
    outb(0x43, 0x36);

    // The divisor has to be sent byte-wise, so we must split it into upper and lower bytes.
    u8int lower = (u8int)(divisor & 0xFF);
    u8int higher = (u8int)((divisor >> 8) & 0xFF);

    // Sending the frequency divisor.
    outb(0x40, lower);
    outb(0x40, higher);

}

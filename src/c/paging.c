/**
  Implementing paging.
**/

#include "../h/paging.h"
#include "../h/kheap.h"

// Kernel's page page_directory
page_directory_t *kernel_directory = 0;

// Current page page_directory
page_directory_t *current_directory = 0;

// Bitset of frames.
u32int *frames;
u32int n_frames;

// Defined in kheap.c
extern u32int placement_address;
extern heap_t *kheap;

// Macros used in the bitset algorithms.
#define INDEX_FROM_BIT(a) (a / (8 * 4))
#define OFFSET_FROM_BIT(a) (a % (8 * 4))

/**
  Setting a bit in the frame bitset
**/
static void set_frame(u32int frame_addr)
{
    u32int frame = frame_addr / 0x1000;

    u32int index = INDEX_FROM_BIT(frame);
    u32int offset = OFFSET_FROM_BIT(frame);

    frames [index] |= (0x1 << offset);
}

/**
  Clearing a bit in the frame bitset.
**/
static void clear_frame(u32int frame_addr)
{
    u32int frame = frame_addr / 0x1000;

    u32int index = INDEX_FROM_BIT(frame);
    u32int offset = OFFSET_FROM_BIT(frame);

    frames [index] &= ~(0x1 << offset);
}

/**
  Testing if a bit is set.
**/
static u32int test_frame(u32int frame_addr)
{
    u32int frame = frame_addr / 0x1000;

    u32int index = INDEX_FROM_BIT(frame);
    u32int offset = OFFSET_FROM_BIT(frame);

    return (frames [index] & (0x1 << offset));
}

/**
  Finding the first frame.
**/
static u32int first_frame()
{
    u32int i, j;

    for (i = 0; i < INDEX_FROM_BIT(n_frames); i ++)
    {
        if (frames [i] != 0xFFFFFFFF)
        {
            for (j = 0; j < 32; j ++)
            {
                u32int to_test = 0x1 << j;

                if (!(frames [i] & to_test))
                    return i * 4 * 8 + j;
            }
        }
    }
}

/**
  Allocating a frame.
**/
void alloc_frame(page_t *page, int is_kernel, int is_writeable)
{
    if (page -> frame != 0)
        return;
    else
    {
        u32int index = first_frame();

        if (index == (u32int) - 1)
        {
            // PANIC is a macro which prints a message to the screen and then hits an infinite loop.
            PANIC("No free frames!");
        }

        set_frame(index * 0x1000);

        page -> present = 1;
        page -> rw = (is_writeable) ? 1:0;        // Should the page be writeable ?
        page -> user = (is_kernel) ? 0:1;         // Should the page be user-mode ?
        page -> frame = index;
    }
}

/**
  Deallocating a frame.
**/
void free_frame(page_t *page)
{
    u32int frame;

    if (!(frame = page -> frame))
    {
        return;
    }
    else
    {
        clear_frame(frame);
        page -> frame = 0x0;
    }
}

/**
  Paging initialization.
**/
void initialize_paging()
{
    // The size of the physical memory (Assuming it's 16MB for the moment).
    u32int mem_end_page = 0x1000000;

    n_frames = mem_end_page / 0x1000;
    frames = (u32int *) kmalloc(INDEX_FROM_BIT(n_frames));
    memset(frames, 0, INDEX_FROM_BIT(n_frames));

    // Making a page directory.
    kernel_directory = (page_directory_t *) kmalloc_a(sizeof(page_directory_t));
    memset(kernel_directory, 0, sizeof(page_directory_t));
    current_directory = kernel_directory;

    // Map some pages in the kernel heap area.
    // Here we call get_page but not alloc_frame. This causes page_table_t's
    // to be created where necessary. We can't allocate frames yet because they
    // they need to be identity mapped first below, and yet we can't increase
    // placement_address between identity mapping and enabling the heap!
    int i = 0;
    for (i = KHEAP_START; i < KHEAP_START + KHEAP_INITIAL_SIZE; i += 0x1000)
        get_page(i, 1, kernel_directory);

    // We need to identity map (phys addr = virt addr) from
    // 0x0 to the end of used memory, so we can access this
    // transparently, as if paging wasn't enabled.
    // NOTE that we use a while loop here deliberately.
    // inside the loop body we actually change placement_address
    // by calling kmalloc(). A while loop causes this to be
    // computed on-the-fly rather than once at the start.
    i = 0;

    while (i < placement_address + 0x1000)
    {
        // Making kernel code not writable from userspace.
        alloc_frame(get_page(i, 1, kernel_directory), 0, 0);

        i += 0x1000;
    }

    // Now allocate those pages we mapped earlier.
    for (i = KHEAP_START; i < KHEAP_START + KHEAP_INITIAL_SIZE; i += 0x1000)
        alloc_frame( get_page(i, 1, kernel_directory), 0, 0);

    // Before enabling paging, we must register the page fault handler.
    register_interrupt_handler(14, page_fault);

    // Enabling paging.
    switch_page_directory(kernel_directory);

    // Initialise the kernel heap.
    kheap = create_heap(KHEAP_START, KHEAP_START + KHEAP_INITIAL_SIZE, 0xCFFFF000, 0, 0);
}


void switch_page_directory(page_directory_t *dir)
{
    current_directory = dir;

    asm volatile("mov %0, %%cr3" :: "r"(&dir -> tables_physical));
    u32int cr0;
    asm volatile("mov %%cr0, %0" : "=r"(cr0));
    cr0 |= 0x80000000;          // Enabling paging!
    asm volatile("mov %0, %%cr0" :: "r"(cr0));
}

page_t *get_page(u32int address, int make, page_directory_t *dir)
{
    // Turning the address into an index.
    address /= 0x1000;

    // Finding the page table containing this address.
    u32int table_index = address / 1024;
    if (dir -> tables [table_index])
    {
        return &dir -> tables [table_index] -> pages [address % 1024];
    }
    else if (make)
    {
        u32int tmp;

        dir -> tables [table_index] = (page_table_t*) kmalloc_ap(sizeof(page_table_t), &tmp);
        memset(dir -> tables [table_index], 0, 0x1000);
        dir -> tables_physical [table_index] = tmp | 0x7;

        return &dir -> tables [table_index] -> pages [address % 1024];
    }
    else
        return 0;
}


/**
  Page fault handler.
**/
void page_fault(registers_t regs)
{
    // When a page fault occurs, the faulting address gets stored into the CR2 register.
    u32int faulting_address;

    asm volatile("mov %%cr2, %0" : "=r" (faulting_address));

    int present = !(regs.err_code & 0x1);       // Page not present.
    int rw = regs.err_code & 0x2;               // Write operation?
    int us = regs.err_code & 0x4;               // Processor was in user mode?
    int reserved = regs.err_code & 0x8;         // Overwritten CPU-reserved bits of page entry?
    int id = regs.err_code & 0x10;              // Caused an instruction fetch?

    monitor_write("Page Fault! (");

    if (present)
        monitor_write("Present ");
    if (rw)
        monitor_write("Read Only ");
    if (us)
        monitor_write("User Mode ");
    if (reserved)
        monitor_write("Reserved ");

    monitor_write(") at 0x");
    monitor_write_hex(faulting_address);
    monitor_write("\n");
    PANIC("Page fault");
}

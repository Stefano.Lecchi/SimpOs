SRCFILES := $(wildcard *.o)

.PHONY: all

all: $(SRCFILES)
	i686-elf-gcc -ffreestanding -nostdlib -g -T linker.ld $(SRCFILES) -o mykernel.elf -lgcc
